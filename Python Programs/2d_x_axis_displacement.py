v = float(input("Velocity of Object: "))
t = float(input("Time of Travel in Seconds: "))
a = float(input("Acceleration of Object: "))

x = (v * t) + .5 * (a * (t**2))
print("Displacement on the x-axis is ",x," meters.")